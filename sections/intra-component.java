1. public List{} getIntraAgglomeration(Set dc, int agglomThreshold){
2. IntraAgglomerations = {}
3. anomalousElems = {}
4.    
5. for(each design component dc in the program){
6.  anomalousElems.addAll(getAnomalousElemsOfComponent(dc))
7. }
8.
9. //Adding anomalous code elements as vertices in the graph
10. graphIntraElems = new graphIntraElems().addVertex.addAll(anomalousElems) 
11.
12. for(each e1 in anomalousElems){
13.   for(each e2 in anomalousElems){   
14.   //Creating the Intra-component graph 
15.     if(isRelated(e1,e2) && (e1.getDesignComponent() == e2.getDesignComponent())){
16.       graphIntraElems.addEdge(e1,e2)
17.     }
18.   }
19. }
20. 
21. // Adding the connected components as agglomerations            
22. IntraAgg = new IntraAgg().addAll(getConnectedComponents(graphIntraElems))
23.
24. // Selecting only agglomerations above the predefined threshold
25. for(each a in IntraAgg){
26.   if(size(a.getAnomalies()) > agglomThreshold){
27.     IntraAgglomerations.add(a)
28.   }
29. }
30. return IntraAgglomerations
31. }