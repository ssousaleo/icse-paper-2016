1. public List{} getInterAgglomeration(Set dc, int agglomThreshold){
2. InterAgglomerations = {}
3. anomalousElems = {}
4.    
5. for(each design component dc in the program){
6.  anomalousElems.addAll(getAnomalousElemsOfComp(dc))
7. }
8.
9. //Adding anomalous code elements as vertices in the graph
10. graphInterElems = new graphInterElems().addVertex.addAll(anomalousElems) 
11.
12. for(each e1 in anomalousElems){
13.   for(each e2 in anomalousElems){   
14.   //Creating the Inter-component graph 
15.     if(isRelated(e1,e2) && (e1.getDesignComponent() != e2.getDesignComponent())){
16.       graphInterElems.addEdge(e1,e2)
17.     }
18.   }
19. }
20. 
21. // Adding the connected components as agglomerations            
22. InterAgg = new InterAgg().addAll(getConnectedComponents(graphInterElems))
23.
24. // Selecting only agglomerations above the predefined threshold
25. for(each a in InterAgg){
26.   if(size(a.getAnomalies()) > agglomThreshold){
27.     InterAgglomerations.add(a)}
28. }
29. return InterAgglomerations
30. }