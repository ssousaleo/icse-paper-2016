1. public List[] getSemanticAgglomeration(Set dc, Set con, int agglomThreshold, int weakThreshold){
2. semanticAgglomerations = []
3. 
4. for(each design concern con in the program){
5.   W = weakDedicatedComponents(con, weakThreshold)
6.   agglomeration = []
7.   if(size(W) > 0){
8.     ae = getAnomalousElemsPerConcern(W, con)
9.     agglomeration.addAll(ae)
10.   }
11.   if(size(agglomeration) > agglomThreshold){
12.     semanticAgglomerations.add(agglomeration)
13.   }
15. }
15. return semanticAgglomerations
16. }