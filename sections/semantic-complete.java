1.  public List[] getConcernAgglomeration(Set dc, Set con, int agglomThreshold, int weakThreshold){
2.    concernAgglomerations = []
3.    
4.    //Searching for overload of concerns
5.    for(each design component dc in the program){
6.      ae = getAnomalousElementsOfComponet(dc)
7.      agglomeration = []
8.
9.      for(each elem in ae){
10.        if(size(elem.getConcerns()) > concernThreshold){
11.          agglomeration.add(elem)
12.       }
13.     }
14.   
15.     if(size(agglomeration.getAnomalies()) > agglomerationThreshold){
16.       concernAgglomerations.add(agglomeration)
17.     }
18.   }
19.   
20.   //Searching for cross-cutting concerns
21.   for(each design concern con in the program){
22.     W = weakDedicatedComponents(con, weakThreshold)
23.     agglomeration = []
24.
25.     if(size(W) > 0){
26.       ae = getAnomalousElemsPerConcern(W, con)
27.       agglomeration.addAll(ae)
28.     }
29.
30.     if(size(agglomeration) > agglomThreshold){
31.       semanticAgglomerations.add(agglomeration)
32.     }
33.   }
34.   
35.   return semanticAgglomerations
36. }