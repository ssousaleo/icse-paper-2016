\section{Results and Analysis}
\label{results}
This section presents the results and analysis of our study. \Cref{exploring} discusses whether syntactic agglomerations assist the location of design problems. Then, \Cref{suffice} analyzes to what extent syntactic agglomerations suffice to locate all design problems and, if not, whether semantic agglomerations can further improve this task. \Cref{earlydetection} discusses if agglomerations can also be effective to assist the location of design problems in early versions of a system.

\subsection{Exploring Syntactic Agglomerations}
\label{exploring}

Before answering our first research question, we reflect upon the relation of syntactic agglomerations and design problems. First, we check whether syntactic agglomerations are more related to design problems than non-agglomerated code anomalies. Non-agglomerated anomalies are ``detached'' anomalies that do not take part of any agglomeration. 

The results are presented in the first three columns of \Cref{tab:statistical}. The first column (named Agglomeration) lists the agglomeration types analyzed in our study. The second column (named AG-DP) shows the amount of agglomerations (for each category) related to design problems. The third column (named NoAG-NoDP) presents the number of ``detached'' anomalies that are irrelevant to locate design problems, i.e. they are not related to any design problem. We discuss the data of the last two columns later in this section. The data of the row ``semantic'' is discussed in \Cref{suffice}.


\begin{table}[]
\centering
\caption{Contingency Table and Fisher's Test Results}
\label{tab:statistical}
\resizebox{\columnwidth}{!}{%
\begin{tabular}{ccccc}
\hline
{\bf Agglomeration} & {\bf AG-DP} & {\bf NoAG-NoDP} & {\bf p-value}     & {\bf ORs} \\ \hline
Intra-component       & 247        & 3996         & \textless 0.001 & 5,669636  \\ \hline
Inter-component       & 167        & 4207         & \textless 0.001 & 4,878982  \\ \hline
Syntactic       & 296        & 3759         & \textless 0.001 & 5,513531  \\ \hline
Semantic       & 97         & 4463         & \textless 0.001 & 7,392637  \\ \hline
\textbf{All }    & \textbf{312}        & \textbf{4596}         & \textless \textbf{0.001} & \textbf{2,999686}  \\ \hline
\end{tabular}
}
\end{table}

\textbf{Reducing the Search Space for Design Problems}. \Cref{tab:statistical} confirms that syntactic agglomerations are good indicators of design problems. Almost 300 syntactic agglomerations are related to design problems. Each row shows how many ``detached'' anomalies could be discarded when developers are exploring a specific type of syntactic agglomeration in order to look for design problems. For instance, if developers are inspecting inter-component agglomerations, they will be able to: (i) find 167 design problems in their systems, and (ii) discard more than 4000 code anomalies returned by their static analysis tools. This result suggests that syntactic agglomerations can assist developers in locating design problems. When identifying design problems in the source code, the scope of analysis can be reduced to the list of code anomalies taking part in the agglomeration, rather than an unmanageable list of individual code anomalies. 

\textbf{Syntactic Agglomerations as Design Problems: \\ \noindent Strength of the Relation.} Even though the previous analysis is interesting, it does not consider that anomaly agglomerations have higher probability of being related to design problems than a single code anomaly, since agglomeration involves more code. The latter affects a single code element (a method or a class), while the former involves at least two code elements. The size of an agglomeration ranged from 2 to 9 in all systems. Therefore, we investigated the strength of the relation between agglomerations and design problems. We performed further analyses to check to what extent various code elements of the agglomeration are, in fact, contributing to the realization of a design problem.

First, we calculated the Odds Ratio \cite{Cornfield1951} for each type of agglomeration (ORs column of \Cref{tab:statistical}), which shows how much higher is the chance of code elements taking part in agglomerations to be related to design problems than individual anomalous elements. The chance of each anomaly within a syntactic agglomeration being related to a design problem is more than five times (5,513) higher than each ``detached'' anomaly. Similar results are observed for both intra-component and inter-component syntactic agglomerations. Second, we also observe that almost all syntactic agglomerations had at least 3 anomalous code elements simultaneously related to the same single design problem in most of the systems. Thus, when fixing a particular design problem, developers are more likely to reveal several anomalous code elements involved in the refactoring strategy.

\textbf{Syntactic Agglomerations as Design Problems: Statistical Significance}. Finally, we used the Fisher's exact test to analyze the statistical significance of the relation between syntactic agglomerations and design problems. The fourth column (p-value) in \Cref{tab:statistical} shows this correlation for each agglomeration type. We assume a confidence level higher than 99\% (p-value threshold = 0.001) as the threshold value to the significant level of the test. Applying the test, we observe that for all agglomeration types the p-value was lower than 0.001. There is a high correlation between both forms of syntactic agglomerations and design problems in the target systems. However, it does not necessarily mean that syntactic agglomerations should not be complemented with other forms of agglomerations to further improve the location of design problems. Therefore, we explicitly address our first research question in the next section.


\begin{table*}[]
\centering
\begin{threeparttable}
\caption{Relation of Agglomerations and Design Problems: Precision and Recall}
\label{tab:comparison}
%\resizebox{\textwidth}{!}{%
\begin{tabular}{c|ccc|ccc|ccc|ccc}
\hline
Agglom. & \multicolumn{3}{c|}{Intra-component}       & \multicolumn{3}{c|}{Inter-component}       & \multicolumn{3}{c|}{Syntactic}             & \multicolumn{3}{c}{Semantic}      \\ \hline
System        & P         & R             & DP     & P         & R             & DP     & P         & R             & DP (*)     & P         & R             & DP (**)  \\ \hline
OODT          & 62\%         & 30\%         & 196     & 73\%         & 83\%         & 549    & 70\%         & 97\%         & 640 (535)    & 91\%         & 65\%          & 431 (201) \\ \hline
MM            & 42\%         & 31\%         & 12     & 82\%         & 23\%         & 9      & 57\%         & 55\%         & 21 (21)     & 100\%        & 13\%          &    5   (3)\\ \hline
HW            & 39\%         & 11\%         & 22     & 45\%         & 87\%         & 163    & 44\%         & 87\%         & 163 (141)    & 75\%         & 100\%         & 187 (24) \\ \hline
S1            & 43\%         & 23\%         & 64     & 51\%         & 21\%         & 58     & 47\%         & 37\%         & 104 (86)    & 81\%         & 12\%         & 34 (18)  \\ \hline
S2            & 38\%         & 9\%         & 77     & 40\%         & 7\%         & 60     & 39\%         & 13\%         & 110 (83)    & 82\%         & 5\%         & 47 (21)  \\ \hline
S3            & 38\%         & 9\%         & 76     & 39\%         & 6\%         & 58     & 38\%         & 12\%         & 109 (84)    & 75\%         & 6\%         & 50 (25)  \\ \hline
S4            & 66\%         & 89\%         & 66     & 34\%         & 60\%         & 45     & 49\%         & 93\%         & 69 (27)    & 100\%        & 13\%          & 10 (6)  \\ \hline
{\bf Median}  & {\bf 42\%}   & {\bf 23\%}   & {\bf } & {\bf 45\%}   & {\bf 23\%}   & {\bf } & {\bf 47\%}   & {\bf 55\%}   & {\bf } & {\bf 82\%}   & {\bf 13\%}   &     \\ \hline
{\bf SD}      & {\bf 0.118 } & {\bf 0.281} &        & {\bf 0.182 } & {\bf 0.349 } &        & {\bf 0.109 } & {\bf 0.368 } &        & {\bf 0.107 } & {\bf 0.370 } &     \\ \hline
\end{tabular}
%}
\begin{tablenotes}
      \small
      \item *  \thinspace Number of design problems exclusively related either to intra-component or to inter-component agglomerations.
      \item **\thinspace Number of design problems exclusively related to semantic agglomerations.
    \end{tablenotes}
  \end{threeparttable}
\end{table*}



\subsection{Do Syntactic Agglomerations Suffice?}
\label{suffice}

Even though syntactic agglomerations are statistically related to design problems, it is important to understand if syntactic agglomerations alone suffice to locate all design problems. Thus, this section addresses RQ1: ``Are syntactic anomaly agglomerations sufficient indicators of design problems?'' In order to answer RQ1, we first analyzed the proportion of syntactic and semantic agglomerations (un)related to design problems for each target system. We compared how often syntactic and semantic agglomerations relate to design problems. \Cref{tab:comparison} shows, for each agglomeration type, the following measures: (i) \textbf{P}recision, (ii) \textbf{R}ecall, and (iii) absolute number of design problems related to agglomerations (\textbf{DP}). Moreover, for each agglomeration type, \Cref{tab:comparison} shows the median and the standard deviation for the columns \textbf{P} and \textbf{R}. The numbers between parentheses in the \textbf{DP} columns are discussed later in this section.

Precision indicates the fraction of identified agglomerations that are correctly related to design problems. Recall indicates the fraction of design problems successfully identified by agglomerations. We calculate precision and recall measures using the following standard equations:

\noindent\begin{minipage}{.5\linewidth}
\begin{equation}
  \textbf{P} = \frac{TP}{TP + FP}
\label{eq-precision}
\end{equation}
\end{minipage}%
\begin{minipage}{.5\linewidth}
\begin{equation}
  \textbf{R} = \frac{TP}{TP + FN}
\label{eq-recall}
\end{equation}
\end{minipage}

where, \textit{TP} (true positive) and \textit{FP} (false positive)  encompass all agglomerations that, respectively, are or are not related to design problems. \textit{FN} (false negative) occurs when a group of code elements is affected by a design problem, but none of them is part of an agglomeration.

\Cref{tab:comparison} shows the relation between agglomerations and design problems in terms of precision and recall. The Syntactic column indicates the aggregate results for both types of syntactic agglomerations: intra-component (second column) and inter-component (third column). We highlight that the values of the Syntactic column cannot be directly obtained by the data in the previous columns. In other words, the number of design problems in the fourth column is not the result of summing the numbers of design problems for both types of syntactic agglomerations. The reason is that there are intersections between instances of intra- and inter-component agglomerations. That is, an inter-component agglomeration might be composed of anomalies that are also members of one or more intra-component agglomerations. This happens because the search of each type of agglomeration occurs independently. Thus, a design problem might be related to more than one agglomeration.

\textbf{Syntactic Agglomerations Suffice to Indicate Several Design Problems.} \Cref{tab:comparison} indicates that inter-com-ponent agglomerations are helpful for locating more instances of design problems than intra-component agglomerations. In four (out of seven) systems, at least almost half (45\%) of the inter-component agglomerations were related to design problems. However, \Cref{tab:comparison} also reveals that both types of syntactic agglomerations are useful to locate different design problems in most systems. As we expected, these two types of syntactic agglomerations are complementary. This finding can be observed by comparing the numbers of the sub-columns DP from the second to the fourth columns. In the column Syntactic, there are additional numbers between parentheses close to the numbers of design problems (sub-column DP). These numbers represent the amount of design problems that were exclusively related to either intra-component or inter-component agglomerations. Therefore, the subsets of design problems related to intra- and inter-component agglomerations in six systems are significantly different. The only exception was the Health Watcher. The reason is that all the design problems found in it were related to the communication between two components.

\textbf{Semantic Agglomerations are Consistent Indicators.} Semantic agglomerations were the most consistent indicators of design problems. This finding is based on the fact that semantic agglomerations were a good indicator of design problems across all the systems. The 5th column (ORs) of \Cref{tab:statistical} shows that the chance of each anomaly within a semantic agglomeration being related to a design problem is more than seven times higher than each ``detached'' anomaly. This likelihood is higher than anomalies within syntactic agglomerations. In addition, semantic agglomerations presented the highest correlation with design problems, when compared to all other syntactic agglomerations. For example, 91\% of the semantic agglomerations in OODT were related to 431 instances of design problems. 

Semantic agglomerations are also often related to design problems even for the other systems where the absolute number of related design problems is lower: (i) the percentage of true positives was high (from 75\% to 100\%), and (ii) the percentage of false positives was low (from 0\% to 25\%). Considering the data from all systems, we observed a median of 82\% of semantic agglomerations related to design problems. The standard deviation of 10.74\% is low as well. That is, the percentage of semantic agglomerations related to design problems was high in all the target systems. Regarding the absolute number of design problems (DP), the semantic agglomerations were related to a high number of design problems in all the systems. 

\textbf{Syntactic and Semantic Agglomerations are Complementary.} Considering each type of agglomeration, recall values were below 60\% for most of the target systems (\Cref{tab:comparison}). Our results suggests that this occurs because syntactic and semantic agglomerations are complementary. In the last column of \Cref{tab:comparison} (DP), there are additional numbers between parentheses close to the numbers of design problems. These numbers represent the amount of design problems exclusively related to semantic agglomerations, i.e., design problems that could not be located only with syntactic agglomerations. Figure~\ref{fig:tec:mobilemedia} shows an example of design problem located only in the context of semantic agglomeration.

Approximately 50\% of the design problems related to semantic agglomerations have no relation to syntactic agglomerations in most of the systems. These design problems were often cases of \textit{Scattered Concerns}, \textit{Component Overload}, \textit{Overused Interface} and \textit{Delegating Abstraction}. This result suggests that semantic agglomerations are useful to complete the location of several design problems. These design problems are also hard to be located by developers as there is no syntactic relationship among the anomalous elements forming the agglomerations.  However, existing work only focuses on characterizing code anomalies based on their syntactic relationships \cite{Abbes2011,Isela2013,Moha2010,Sjoberg2013}. Moreover, these design problems cannot be detected through the use of only concern location techniques. Several crosscutting concerns in the analyzed systems are not harmful, i.e., their implementations did not contain code anomalies and they were not actual sources of design problems. 

\textbf{Design Problems Indirectly Related to Agglomerations.} We observed occurrences of design problems affecting methods or classes that are indirectly related to agglomerations even not participating of one. We observed three recurrent patterns of indirect relation: two for methods and one for classes. The first occurs when a method is implemented inside a class that is part of an agglomeration. The second is observed when a method is implemented in a class that have another method in an agglomeration. Finally, the third occurs when a class have one or more methods in agglomerations. We expect an increase in the recall values because, considering indirect relation, makes the correlation rule less strict. For instance, in S3 the recall value for semantic agglomerations increases from 6\% to 51\%. This suggests that an agglomeration may be helpful to identify design problems even without a direct relation. This becomes possible when developers analyze the full context of agglomerations, instead of analyzing anomalies individually. 

\textbf{Reducing False Positives of Syntactic Agglomerations.} Even though syntactic agglomerations are often related to design problems, developers would still have to inspect and discard several irrelevant agglomerations. \Cref{tab:comparison} shows that the number of false positives is higher than 60\% in two systems. However, we observed that the combined use of semantic and syntactic agglomerations would significantly reduce the number of false positives yielded by the use of only syntactic agglomerations. If we only consider syntactic agglomerations that intersect with semantic agglomerations, the number of false positives for syntactic agglomerations would be reduced to 21\% or less in all the systems. There is an intersection between a syntactic agglomeration X and a semantic agglomeration Y when at least one anomalous elements take part of both X and Y. In summary, the joint use of syntactic and semantic agglomerations significantly enhances the location of design problems.

\textbf{Full Automation of Semantic Agglomerations?} The computation of semantic agglomerations relies on mapping of system concerns to indicate design problems. Tables \ref{tab:statistical} and \ref{tab:comparison} show the results for semantic agglomerations computed with concern mappings produced by the systems' developers. However, it is important to check whether semantic agglomerations can also improve the location of design problems when concern mappings are automatically generated. As mentioned in Section \ref{collection}, we also relied on concern mappings provided by Mallet in order to generate a second set of semantic agglomerations.  Comparing to the developers' mapping, we noticed Mallet generates longer lists of concerns and concern mappings. As a consequence, the use of Mallet results in a higher number of semantic agglomerations, which was related to more design problems than our original computation (\Cref{tab:comparison}). Semantic agglomerations generated with Mallet's output lead to an average increase of 39.58\% in the identification of design problems when compared to semantic agglomerations generated with developers' mappings. The Mallet configuration and all the detailed results are available in our supplementary material \cite{opus}.

\subsection{Agglomerations as Congenital Problems?}
\label{earlydetection}

This section addresses the RQ2: ``What proportion of design problems early manifest themselves as agglomerations?'' Early manifestation means the design problems were present in the first versions of a system. Therefore, they are likely to represent ``congenital'' design problems, i.e., they were introduced at design time. In order to answer this question, we computed for all the systems: (i) the number of design problems in the first versions of our target systems, and (ii) the proportion of design problems related to agglomerations.

The analysis of all initial versions revealed that a considerable number of design problems was introduced in the first version of each target system. As opposed to our expectation, all the initial versions presented a high correlation between agglomerations and design problems. 
We expected most of the design problems would be ``evolutionary'', i.e., they would be introduced during the system evolution. However, the proportion of design problems related to agglomerations in early versions was approximately 75\% for MM, S1, S2 and S4. The proportion for the other systems was close to 65\%. We also observed that a considerable proportion of design problems are exclusively related to either syntactic or semantic agglomerations already in the first version.

These results show that change history analysis \cite{Girba2007,Schwanke:2013, Wong:2011,XiaoTitan:2014} would not be an effective solution to reveal many instances of design problems. Many design problems are congenital and they are already ``born'' as agglomerations. It would be harder to remove them in later versions, when eventually the agglomeration's anomalous elements start to suffer co-changes through the later versions. An example of this case is the \textit{Fat Interface} in the \textit{Versioning} component in OODT (\Cref{motivation}). It would be cumbersome to restructure the \textit{Versioner} interface in later releases. The number of client classes for this interface increased from one (in the first release) to more than fifteen (last release).

\textbf{Comparing Early and Late Versions.} We also compared the nature of design problems and agglomerations in ``early'' and ``late'' versions of MM an HW systems. The comparison of the different versions of these two systems serves to illustrate most of our findings. For instance, let us consider the versions 1 and 8 of the MM system. Version 1 of MM (0.8 KSLOC) is smaller than version 8 (10 KSLOC). Therefore, the number of code anomalies and agglomerations is proportionally smaller in version 1. Even with fewer code elements, the first version already contained 23\% of the agglomerations present in version 8. One of them, for example, is related to the \textit{BaseController} class. In the first version, this class was identified as part of an intra-component agglomeration. That is, this anomalous class was related to another anomalous code element of the same component. This agglomeration was related to the \textit{Component Overload} problem. In the subsequent versions, this agglomeration ``expanded'' to several code elements, including those located in other components. More specifically, in version 8, we identified that the same agglomeration became an inter-component agglomeration, involving classes that use the \textit{BaseController} class. The inter-component agglomeration was related to emerging instances of \textit{Fat Interface} and \textit{Overused Interface} problems.

For the HW system, we compared versions 1 and 10. In this case, version 1 (6 KSLOC) was only somewhat smaller than version 10 (8 KSLOC). However, as in the case of the MM system, we observed again that some agglomerations found in a late version had already started to form in the first version. The first version already contained 39\% of syntactic and semantic agglomerations found in version 10. All these observations suggest that agglomerations are effective to assist the identification and removal of design problems in the early versions of a system, which can prevent the introduction of more severe problems in subsequent versions.