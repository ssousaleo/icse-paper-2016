\section{Study Definition}
\label{study}

Our study investigates whether and how inter-related code anomalies, referred to \textit{anomaly agglomerations}, are related to design problems. \Cref{rq} describes and motivates our research questions. Sections \ref{topologies} and \ref{semantic} present the types of agglomerations we are investigating. \Cref{targetsystems} describes the target systems of our study. Finally, \Cref{collection} addresses the procedure for data collection and analysis.

\subsection{Research Questions}
\label{rq}

Existing techniques \cite{Abbes2011, Moha2010, Sjoberg2013} usually assume that individual anomalies suffice for assisting developers in locating design problems in the program. The previous section showed that each design problem may be realized by various inter-related code anomalies scattered in the program. However, there is no understanding whether certain anomaly relationships can help developers in better locating design problems than individual code anomalies. We are primarily interested in groups of syntactically related code anomalies, i.e., \textit{syntactic agglomerations} for short (\Cref{topologies}). Two anomalies are syntactically related if their host program elements are connected through method calls or inheritance relationships. We first investigate whether syntactic agglomerations often embody design problems in the source code. If this hypothesis holds, the inspection of such agglomerations can help developers in locating design problems. This reasoning leads to our first research question:


\begin{itemize}
\item[RQ1] Are syntactic anomaly agglomerations sufficient indicators of design problems?
\end{itemize}

Syntactic agglomerations may not suffice to locate all design problems. Thus, other forms of code anomaly agglomerations may be required to locate design problems in the source code (\Cref{semantic}). Therefore, we address RQ1 by analyzing the relation between diverse forms of agglomerations and design problems in the context of seven software projects. We also analyze this relation in early versions of each project. Recent studies \cite{Isela2013, tufano2015} revealed that, most of the time, programs are affected by code anomalies since their creation. In addition, it might be that some design problems are ``congenital'', i.e., they manifest in the initial versions of a program (\Cref{motivation}). However, there is no understanding about the relation between ``early'' code anomalies and congenital design problems; and more importantly, between anomaly agglomerations and design problems. This gap motivates our second research question:

\begin{itemize}
\item[RQ2] What proportion of design problems manifest as anomaly agglomerations in early versions of a program? 
\end{itemize}

We address RQ2 by investigating the relation of design problems and code anomaly agglomerations in early versions of a system. If this relation holds, early detection of congenital design problems can be improved by using code anomaly agglomerations. We address RQ2 by analyzing the available initial versions of the analyzed software systems. 


\subsection{Syntactical Agglomerations}
\label{topologies}

A \textit{syntactic agglomeration} is a group of at least two anomalous code elements explicitly related in a program. A code element is \textit{anomalous} when it is affected by one or more code anomalies. In our study, we consider the following code elements: classes, interfaces, methods, constructors, and fields. An explicit relationship is established between two anomalous elements when they have at least one of the following dependencies: shared attribute, method call, class extension or method overload. We classify syntactic agglomerations according to their scope in the program: (i) \textit{intra-component agglomerations}, i.e., those entirely located within a single component, and (ii) \textit{inter-component agglomerations}, i.e., those located in two or more components. In our study, we consider each component is realized by a package in the analyzed Java programs (\Cref{targetsystems}), unless the system developers specify otherwise. Developers may state a component is realized by a set of classes that are not necessarily located in the same package. 

\textbf{Inter-Component Agglomeration} is a syntactic agglomeration that involves two or more design components, with at least one anomalous code element located within each of them. The example in \Cref{motivation} illustrates an inter-component agglomeration in the OODT system. The agglomeration is formed by anomalous code elements located in three OODT components: \textit{Versioning}, \textit{Util} and \textit{System}. In \Cref{alg:versioner}, the inter-component agglomeration is surrounded by the dashed black line, and the green circles represent the code anomalies located in the syntactically related code elements that compose the agglomeration.


\Cref{alg:syntactic} illustrates the algorithm (pseudo-code) for computing inter-component agglomerations. The algorithm has two parameters: (i) \textit{dc}: a set of design components, and (ii) \textit{agglomThreshold}: a threshold value for the minimum agglomeration size. First, the algorithm identifies all anomalous elements of a design component by using the function \textit{getAnomalousElemsOfComp()} (line 6). The anomalous elements of the program are recorded in the \textit{anomalousElems} attribute. The algorithm generates a graph (\textit{graphInterElems}) with the syntactic relationships (edges) between anomalous elements (vertices) located in different design components (line 10 and lines 12--19). To find the relationships between elements, the algorithm uses the function \textit{isRelated()} (line 15). In order to detect the agglomerations, the algorithm finds the subgraphs within the graph in which any two vertices are connected (the subgraph formed by the relationships among the anomalous elements). Finally, only the agglomerations with the size greater than \textit{agglomThreshold} are included in the resulting set of inter-component agglomerations, i.e., \textit{interAgglomerations} (lines 25--29).

\textbf{Intra-Component Agglomeration} is a syntactic agglomeration composed of anomalous code elements located in the same design component. Given space constraints, the algorithm is available in our on-line supplementary material \cite{opus}. However, the algorithm for computing intra-component agglomerations is similar to the previous algorithm for identifying inter-component agglomerations. The only difference is that the algorithm generates a graph with the relationships (edges) between the anomalous elements (vertices) within each design component. 

\lstinputlisting[language=Java,caption=Inter-component Agglomeration Algorithm,label=alg:syntactic]{./sections/syntatic.java}

\subsection{Semantic Agglomerations}
\label{semantic}

Based on the example of \Cref{motivation}, we hypothesized that code anomalies might somehow interact through the program structure because of the presence of a single design problem. In that example, the syntactic relationships amongst the anomalous elements would be sufficient to help developers in revealing the design problem. However, this might not be the case for all occurrences of design problems. In some cases, design problems might be related to semantically connected anomalies. Anomalies are semantically connected if their host elements are addressing the same concern.  Concern is a property or functionality of interest to the designers of a system, but it is not necessarily modularized in a single component. In such cases, the semantic relationship may help the developers to locate certain design problems better than the syntactic relationships.

Let us consider an example extracted from the Mobile Media system \cite{Young2015}. Mobile Media is a software product line to derive applications that manipulate photos, videos and music on mobile devices \cite{Young2015}. Figure~\ref{fig:tec:mobilemedia} depicts a partial view of three components of Mobile Media design: \textit{Controller}, \textit{Screens}, and \textit{Sms}. Classes of the \textit{Screens} component are affected by the \textit{Divergent Change} anomaly, while classes of the \textit{Controller} and \textit{Sms} components were infected by the \textit{Divergent Change} and \textit{Shotgun Surgery} anomalies. These code anomalies did not represent isolate problems. Many of these classes are not syntactically connected, but their methods partially address the same concern, called \textit{Photo Label Management}. Therefore, the anomalous code elements are altogether realizing the design problem \textit{Scattered Concern}, i.e., multiple components realizing a crosscutting concern \cite{Garcia2013,Macia2012}. The realization of \textit{Photo Label Management} should be modularized in the \textit{Controller} component. This problem was the cause of major design refactorings along Mobile Media evolution. However, the design problem would be better spotted if the scattered anomalies (\textit{Divergent Changes} and \textit{Shotgun Surgeries}) realizing the same concern are considered altogether. In our study, we chose concern-based agglomeration as a representative type of semantic agglomerations.

	\begin{figure}[ht]
	\centering
	\includegraphics[width=0.9\columnwidth]{./images/figure-mm-semsetas.png}
	\caption{Scattered Concern in Mobile Media}
	\label{fig:tec:mobilemedia}
	\end{figure}		

\textbf{Semantic Agglomeration} is composed of anomalous code elements realizing a single concern, which is not modularized by design components. Examples of design-relevant concerns can be classified in domain-specific concerns, such as \textit{Photo Label Management}, or general-purpose concerns, such as persistence, error handling, security and the like. Semantic relationships between two or more code anomalies occur when their host code elements are intended to (partially) realize a single design's purpose or concern, i.e., the semantic relationship appears in code elements that implement the same concern and also have code anomalies.

 \Cref{alg:concern-based} presents the algorithm (pseudo-code) for detecting semantic agglomerations. The full algorithm is available in our supplementary material \cite{opus}. The basic idea of the algorithm is searching for agglomerations for each concern \textit{con} that satisfies the following conditions: (i) \textit{con} is located in one or more components (line 7), and (ii) at least one of these components is not mainly dedicated to realize \textit{con} (line 5). The components in (ii) are named \textit{weakly-dedicated components}. The identification of weakly-dedicated components is implemented by the function \textit{weakDedicatedComponents()} (line 5). This function computes, for each component realizing a concern, if code elements in the component are mostly dedicated to realize other concerns. In other words, a component \textit{dc} is weakly dedicated to a concern \textit{con}, if \textit{con} is not the main concern of the code elements within \textit{dc}. Although \textit{dc} partially implements \textit{con}, the predominant concern of \textit{dc} is not \textit{con}. The minimum degree of dedication is captured based on a percentage threshold, named \textit{weakThreshold}.

Thus, the main algorithm uses four inputs: a set of design components \textit{dc}, the mappings for each concern \textit{con}, a threshold for the minimum agglomeration's size \textit{agglomThreshold} (default = 1), and \textit{weakThreshold} (default = 1). In our case study, the developers provided the concern mappings (\Cref{collection}), but the concerns can be automatically extracted from the source code using an automatic concern location tool. Once the concerns are recovered, the algorithm can locate the code elements that implement an concern \textit{con} and also are infected by code anomalies (line 7). The anomalous code elements contributing to weakly-dedicated components of a crosscutting concern are grouped into an agglomeration candidate (lines 8 to 9). If the agglomeration candidate has a number of anomalies higher than the \textit{agglomThreshold} (line 11), then this agglomeration is included in the results (line 12) and confirmed as an actual agglomeration.


\lstinputlisting[language=Java,caption=Semantic Agglomeration Algorithm,label=alg:concern-based]{./sections/semantic.java}

\subsection{Target Systems}
\label{targetsystems}

In order to address the two research questions, we analyzed systems with a wide range of characteristics. We selected 7 systems of different sizes, leveraging different design styles, and spanning different domains. \Cref{tab:targetsystems} summarizes the general characteristics of each system. We focused on these systems because: (i) their designs had degraded, (ii) they present a wide range of design problems, and (iii) their developers were available to provide us with a reliable list of design problems (which are causes of major maintenance effort) and the mappings of their design's concerns.


\begin{table}[]
\centering
\caption{Characteristics of the Target Systems}
\label{tab:targetsystems}
\resizebox{\columnwidth}{!}{%
\begin{tabular}{cccc}
\hline

\textbf{System} & \textbf{Type}                  & \textbf{Design}        & \textbf{KSLOC} \\ \hline
HW     & Web Framework         & Layers        & 8     \\ \hline
MM     & Software Product Line & MVC           & 10    \\ \hline
S1     & Desktop Application   & Client-Server & 122   \\ \hline
S2     & Desktop Application   & Client-Server & 118   \\ \hline
S3     & Desktop Application   & Client-Server & 93    \\ \hline
S4     & Web Application       & MVC           & 116   \\ \hline
OODT   & Middleware            & Layers        & 129   \\ \hline

\end{tabular}
}
\end{table}

The first system is the Health Watcher (HW), a web framework system that allows citizens to register complaints about the health issues in public institutions \cite{Soares2002}. The second is Mobile Media (MM), an academic software product line to derive applications that manipulate photos, videos, and music on mobile devices \cite{Young2015}. The next four systems are proprietary and, due to intellectual-property constraints, we will refer to them as S1, S2, S3 and S4. S1 and S2 manage activities related to production and distribution of oil. S3 manages the trading stock of oil, and S4 supports the financial market analysis. Finally, the last system is Apache OODT, whose goal is to develop and promote the management and storage of scientific data \cite{Mattmann2006}. For all the target systems, several classes implement each component. In OODT, for instance, each design component is implemented by an average of 24 classes. There was always a 1-to-1 mapping between components and packages in three systems: MM, HW, OODT, S3 and S4. For S1 and S2, the developers provided the set of classes implementing each component.


\subsection{Data Collection Procedure}
\label{collection}

The data collection process comprised the following activities: (i) identifying design problems with help of the systems developers, (ii) detecting code anomalies, concern mappings and agglomerations, and (iii) correlating agglomerations and design problems.  Next, we describe each activity.

\textbf{Identifying Design Problems.} We produced a ``ground truth'' of design problems for each target system. We performed two steps to incrementally develop the ground truth. First, original developers of the systems provided us with an initial list of design problems. They listed the problems and explained the relevance of each one through a questionnaire \cite{opus}. They reported which was the maintenance effort caused by the presence of each design problem. They also described which code elements were contributing to the realization of each design problem. Second, we performed other steps to validate the initial list for correctness and completeness. An additional identification of design problems was performed using the source code and the system design. For systems without design documentation, we relied on a suite of design recovery tools \cite{Garcia2013}. The procedure for deriving the list of design problems with developers was the following: (i) an initial list of design problems was identified using detection strategies presented in \cite{Isela2013}, (ii) the developers had to confirm, refute or expand the list of identified  design problems, (iii) the developers provided a brief explanation about the relevance of the design problem, and (iv) when we suspected there was still inaccuracies in the list of design problems, we asked the developers for further feedback. \Cref{tab:designproblems} describes the types of design problems and number of instances identified in our sample of systems.

\begin{table}[]
\centering
\caption{Analyzed Design Problems}
\label{tab:designproblems}
\resizebox{\columnwidth}{!}{%
\begin{tabular}{clr}
\hline
{\bf Name}                                                                  & \multicolumn{1}{c}{{\bf Description}}                                                               & \multicolumn{1}{c}{{\bf Instances}}                                                                                                                           \\ \hline
\begin{tabular}[c]{@{}c@{}}Fat Interface \end{tabular}                & \begin{tabular}[c]{@{}l@{}}Interface of a design component that offers only a\\ general, ambiguous entry-point that provides non-cohesive\\ services, thereby complicating the clients' logic.\end{tabular} & 114 \\ \hline
\begin{tabular}[c]{@{}c@{}}Unwanted Dependency \end{tabular}          & Dependency that violates an intended design rule.                                                                                                               & 2145 \\ \hline
\begin{tabular}[c]{@{}c@{}}Component Overload \end{tabular}               & \begin{tabular}[c]{@{}l@{}}Design components that fulfill too many\\ responsibilities. \end{tabular} & 141                                   \\ \hline
\begin{tabular}[c]{@{}c@{}}Cyclic Dependency \end{tabular}            & \begin{tabular}[c]{@{}l@{}}Two or more design components that directly or\\ indirectly depend on each other.\end{tabular} & 351                                            \\ \hline
\begin{tabular}[c]{@{}c@{}}Delegating Abstraction \end{tabular} & \begin{tabular}[c]{@{}l@{}}An abstraction that exists only for passing messages from\\ one abstraction to another.\end{tabular} & 35                                             \\ \hline
\begin{tabular}[c]{@{}c@{}}Scattered Concern \end{tabular}            & \begin{tabular}[c]{@{}l@{}}Multiple components that are responsible\\ for realizing a crosscutting concern.\end{tabular} & 216                              \\ \hline
\begin{tabular}[c]{@{}c@{}}Overused Interface \end{tabular}           & \begin{tabular}[c]{@{}l@{}}Interface that is overloaded with many clients accessing\\ it. That is, an interface with "too many clients".\end{tabular} & 39          \\ \hline
\begin{tabular}[c]{@{}c@{}}Unused Abstraction \end{tabular}             & \begin{tabular}[c]{@{}l@{}}Design abstraction that is either unreachable or \\ never used in the system.\end{tabular} & 59                                                                                                                    \\ \hline
\end{tabular}
}
\end{table}

\textbf{Concern Mappings.} The initial lists of concerns and their mappings in the source code were provided with the assistance of systems' developers. For each concern in their initial lists, they provided a list of methods or classes realizing those concerns. Given the large size of certain systems, developers could eventually not produce complete concern mappings. Therefore, we also relied on the Mallet \cite{McCallumMALLET} tool. Mallet is a concern location tool that explores topic modeling in order to automatically generate a list of concerns and identify the code elements realizing each concern in a program. Then, we computed and compared two sets of semantic agglomerations: one based on the concern mapping produced by developers, and the other based on the concern mapping generated by Mallet. We will discuss the comparison of such agglomerations in \Cref{suffice}. 

\textbf{Code Anomalies and Agglomerations.} We considered 7 types of code anomalies: \textit{Data Class}, \textit{Divergent Change}, \textit{God Class}, \textit{Shotgun Surgery}, \textit{Feature Envy}, \textit{Long Method} and \textit{Long Parameter List}. They were selected because they represent different types of symptoms at class and method levels. The detection of code anomalies was performed using well-known metrics-based strategies \cite{Lanza2005}. These detection strategies are similar to those typically used in previous empirical studies (e.g. \cite{Macia2012a,Macia2012,Macia2012b,tufano2015}). Such strategies have been proven to be effective for detecting code anomalies in other systems, with precision higher than 80\% \cite{Isela2013,Macia2012}. Once the anomalies were detected, we computed the syntactic and semantic agglomerations. The detection of anomalies and agglomerations were carried out with a tool, called Organic \cite{Macia2012a}. Organic implements: (i) the 7 detection strategies for code anomalies, and (ii) the algorithms for computing agglomerations (\Cref{topologies} and \Cref{semantic}). The results of these steps are available at the supplementary material \cite{opus}.

\textbf{Correlating Agglomerations and Design Problems.} As aforementioned, developers indicated the location of each design problem. In order to answer our research questions, we defined a criterion for correlating design problems with anomalies and agglomerations. A design problem is related to an individual code anomaly if the former is either partially or fully realized by the code element affected by the anomaly. We consider that a design problem and an agglomeration are related if they co-occur in at least two code elements. Even though agglomeration and design problem may be located in more than two anomalous elements, our criterion considers sufficient if they co-occur in two elements. Thus, an agglomeration fails to indicate design problems when either none or only one of its code anomalies is related to a design problem. Finally, we computed the strength of the relation between agglomerations and design problems, i.e., the number of anomalies in agglomeration that contributed (or not) to a design problem (\Cref{exploring}).  We checked the statistical significance of our results using the Fisher's exact test \cite{Fisher1922} and computing the Odds Ratio \cite{Cornfield1951} with the R tool \cite{Bloomfield2014}. 

