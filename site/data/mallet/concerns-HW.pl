mapping('Concern-0', class('lib/exceptions','InsertEntryException')).
mapping('Concern-0', class('lib/exceptions','InvalidDateException')).
mapping('Concern-0', class('lib/exceptions','UpdateEntryException')).
mapping('Concern-0', class('lib/util','Date')).
mapping('Concern-0', class('lib/util','Library')).
mapping('Concern-0', class('lib/util','Schedule')).
mapping('Concern-1', class('healthwatcher','Constants')).
mapping('Concern-1', class('healthwatcher/model/employee','Employee')).
mapping('Concern-1', class('healthwatcher/view/servlets','ServletConfigRMI')).
mapping('Concern-1', class('healthwatcher/view/servlets','ServletInsertEmployee')).
mapping('Concern-1', class('healthwatcher/view/servlets','ServletLogin')).
mapping('Concern-1', class('healthwatcher/view/servlets','ServletSearchDiseaseData')).
mapping('Concern-1', class('healthwatcher/view/servlets','ServletSearchHealthUnitsBySpecialty')).
mapping('Concern-1', class('healthwatcher/view/servlets','ServletSearchSpecialtiesByHealthUnit')).
mapping('Concern-1', class('healthwatcher/view/servlets','ServletUpdateComplaintData')).
mapping('Concern-1', class('healthwatcher/view/servlets','ServletUpdateEmployeeData')).
mapping('Concern-1', class('healthwatcher/view/servlets','ServletUpdateEmployeeSearch')).
mapping('Concern-1', class('healthwatcher/view/servlets','ServletUpdateHealthUnitData')).
mapping('Concern-1', class('healthwatcher/view/servlets','ServletWebServer')).
mapping('Concern-1', class('lib/exceptions','InvalidSessionException')).
mapping('Concern-2', class('healthwatcher/business/complaint','ComplaintRecord')).
mapping('Concern-2', class('healthwatcher/model/complaint','AnimalComplaint')).
mapping('Concern-2', class('healthwatcher/model/complaint','Complaint')).
mapping('Concern-2', class('healthwatcher/model/complaint','FoodComplaint')).
mapping('Concern-2', class('healthwatcher/model/complaint','SpecialComplaint')).
mapping('Concern-2', class('healthwatcher/view/servlets','ServletInsertAnimalComplaint')).
mapping('Concern-2', class('healthwatcher/view/servlets','ServletInsertFoodComplaint')).
mapping('Concern-2', class('healthwatcher/view/servlets','ServletInsertSpecialComplaint')).
mapping('Concern-2', class('healthwatcher/view/servlets','ServletSearchComplaintData')).
mapping('Concern-3', class('healthwatcher/data/rdb','AddressRepositoryRDB')).
mapping('Concern-3', class('healthwatcher/data/rdb','ComplaintRepositoryRDB')).
mapping('Concern-3', class('healthwatcher/data/rdb','DiseaseTypeRepositoryRDB')).
mapping('Concern-3', class('healthwatcher/data/rdb','EmployeeRepositoryRDB')).
mapping('Concern-3', class('healthwatcher/data/rdb','HealthUnitRepositoryRDB')).
mapping('Concern-3', class('healthwatcher/data/rdb','SpecialityRepositoryRDB')).
mapping('Concern-4', class('healthwatcher/business','HealthWatcherFacade')).
mapping('Concern-4', class('healthwatcher/business/employee','EmployeeRecord')).
mapping('Concern-4', class('healthwatcher/data','IAddressRepository')).
mapping('Concern-4', class('healthwatcher/data','IComplaintRepository')).
mapping('Concern-4', class('healthwatcher/data','IDiseaseRepository')).
mapping('Concern-4', class('healthwatcher/data','IEmployeeRepository')).
mapping('Concern-4', class('healthwatcher/data','IHealthUnitRepository')).
mapping('Concern-4', class('healthwatcher/data','ISpecialityRepository')).
mapping('Concern-4', class('healthwatcher/data','ISymptomRepository')).
mapping('Concern-4', class('healthwatcher/data/mem','ComplaintRepositoryArray')).
mapping('Concern-4', class('healthwatcher/data/mem','DiseaseTypeRepositoryArray')).
mapping('Concern-4', class('healthwatcher/data/mem','EmployeeRepositoryArray')).
mapping('Concern-4', class('healthwatcher/data/mem','HealthUnitRepositoryArray')).
mapping('Concern-4', class('healthwatcher/data/mem','SpecialityRepositoryArray')).
mapping('Concern-4', class('healthwatcher/data/mem','SymptomRepositoryArray')).
mapping('Concern-4', class('healthwatcher/view','IFacade')).
mapping('Concern-4', class('lib/distribution/rmi','IIteratorRMITargetAdapter')).
mapping('Concern-4', class('lib/distribution/rmi','IteratorRMISourceAdapter')).
mapping('Concern-4', class('lib/distribution/rmi','IteratorRMITargetAdapter')).
mapping('Concern-4', class('lib/exceptions','ObjectAlreadyInsertedException')).
mapping('Concern-4', class('lib/exceptions','ObjectNotFoundException')).
mapping('Concern-4', class('lib/util','ConcreteIterator')).
mapping('Concern-4', class('lib/util','IteratorDsk')).
mapping('Concern-4', class('lib/util','LocalIterator')).
mapping('Concern-5', class('healthwatcher/model/address','Address')).
mapping('Concern-5', class('healthwatcher/view/servlets','HWServlet')).
mapping('Concern-5', class('lib/concurrency','ConcurrencyManager')).
mapping('Concern-5', class('lib/exceptions','CommunicationException')).
mapping('Concern-5', class('lib/exceptions','ExceptionMessages')).
mapping('Concern-5', class('lib/exceptions','ObjectNotValidException')).
mapping('Concern-5', class('lib/exceptions','PersistenceMechanismException')).
mapping('Concern-5', class('lib/exceptions','PersistenceSoftException')).
mapping('Concern-5', class('lib/exceptions','RepositoryException')).
mapping('Concern-5', class('lib/exceptions','SituationFacadeException')).
mapping('Concern-5', class('lib/persistence','IPersistenceMechanism')).
mapping('Concern-5', class('lib/persistence','PersistenceMechanism')).
mapping('Concern-5', class('lib/util','Functions')).
mapping('Concern-6', class('healthwatcher/model/complaint','DiseaseType')).
mapping('Concern-6', class('healthwatcher/model/complaint','Situation')).
mapping('Concern-6', class('healthwatcher/model/complaint','Symptom')).
mapping('Concern-6', class('healthwatcher/model/healthguide','MedicalSpeciality')).
mapping('Concern-6', class('healthwatcher/view/servlets','ServletGetDataForSearchByDiseaseType')).
mapping('Concern-6', class('healthwatcher/view/servlets','ServletGetDataForSearchByHealthUnit')).
mapping('Concern-6', class('healthwatcher/view/servlets','ServletGetDataForSearchBySpeciality')).
mapping('Concern-6', class('healthwatcher/view/servlets','ServletUpdateComplaintSearch')).
mapping('Concern-6', class('healthwatcher/view/servlets','ServletUpdateHealthUnitSearch')).
mapping('Concern-6', class('lib/util','HTMLCode')).
mapping('Concern-7', class('healthwatcher/business','HealthWatcherFacadeInit')).
mapping('Concern-7', class('healthwatcher/business/complaint','DiseaseRecord')).
mapping('Concern-7', class('healthwatcher/business/healthguide','HealthUnitRecord')).
mapping('Concern-7', class('healthwatcher/business/healthguide','MedicalSpecialityRecord')).
mapping('Concern-7', class('healthwatcher/model/healthguide','HealthUnit')).
mapping('Concern-7', class('lib/exceptions','TransactionException')).
